# Class Comparison Examples

This is a small project to show some examples of class comparison in LabVIEW. Feel free to contribute with more examples.

## Author

Felipe Pinheiro Silva

## Contribution

Merge requests are welcome. Open an issue first.

### Environment

It was developed using LabVIEW 2020 Community Edition SP1. 

### Testing

This project uses the Caraya Unit Test Framework for unit testing and validating the examples.
