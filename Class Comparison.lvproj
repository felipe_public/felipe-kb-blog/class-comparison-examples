﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Source" Type="Folder">
			<Item Name="Child Class One" Type="Folder">
				<Item Name="Child One.lvclass" Type="LVClass" URL="../src/Child One/Child One.lvclass"/>
			</Item>
			<Item Name="Class One" Type="Folder">
				<Item Name="Class One.lvclass" Type="LVClass" URL="../src/Class One/Class One.lvclass"/>
			</Item>
			<Item Name="Class One Lib" Type="Folder">
				<Item Name="Class One Lib.lvlib" Type="Library" URL="../src/Class One Lib/Class One Lib.lvlib"/>
			</Item>
			<Item Name="Class Two" Type="Folder">
				<Item Name="Class Two.lvclass" Type="LVClass" URL="../src/Class Two/Class Two.lvclass"/>
			</Item>
		</Item>
		<Item Name="Tests" Type="Folder">
			<Item Name="Unit" Type="Folder">
				<Item Name="Class Comparison UT.lvlib" Type="Library" URL="../tests/unit/Class Comparison UT.lvlib"/>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Array Size(s)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Array to Array of VData__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Array to Array of VData__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Error Cluster__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path - File Names Array - path__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path - File Names Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path - Traditional - path__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path - Traditional__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Build Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Build Path__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Cluster to Array of VData__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Delete Elements from Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 1D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty 2D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Empty Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Empty Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663File Exists - Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663File Exists - Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663File Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663File Exists - Scalar__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663File Exists__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663File Exists__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Filter 1D Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Filter 1D Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Format Variant Into String__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Format Variant Into String__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Array Element TDEnum__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Data Name from TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Data Name__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Data Name__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Header from TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Last PString__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Last PString__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Physical Units from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Physical Units from TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Physical Units__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Physical Units__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get PString__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get PString__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Refnum Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Refnum Type Enum from Data__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Refnum Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Refnum Type Enum from TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Strings from Enum__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get TDEnum from Data__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get TDEnum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get TDEnum from TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Variant Attributes__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Waveform Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Waveform Type Enum from Data__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Get Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Get Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663List Directory Recursive__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663List Directory Recursive__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663List Directory__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663List Directory__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Parse String with TDs__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Physical Units__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Physical Units__ogtk.ctl"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Refnum Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Refnum Subtype Enum__ogtk.ctl"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reorder Array2__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Reshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Reshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Resolve Timestamp Format__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Resolve Timestamp Format__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Search Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Search Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Set Data Name__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Set Data Name__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Sort Array__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Sort Array__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Split Cluster TD__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - 1D Array of Paths__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - 1D Array of Paths__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - 1D Array of Strings__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - 1D Array of Strings__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - Path__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - Path__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - String__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Strip Path Extension - String__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Strip Path Extension__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Strip Path Extension__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Strip Units__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Strip Units__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Trim Whitespace__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Type Descriptor Header__ogtk.ctl"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Type Descriptor__ogtk.ctl"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Variant to Header Info__ogtk.vi"/>
				<Item Name="059DC194CAFE701435A8C0AF06F6A663Waveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/_Caraya_internal_deps/059DC194CAFE701435A8C0AF06F6A663Waveform Subtype Enum__ogtk.ctl"/>
				<Item Name="Add State(s) to Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Add State(s) to Queue__jki_lib_state_machine.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Caraya Interactive Menu.rtm" Type="Document" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/Caraya/menu/Caraya Interactive Menu.rtm"/>
				<Item Name="Caraya.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/Caraya/Caraya.lvlib"/>
				<Item Name="CFReleaseString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFString.llb/CFReleaseString.vi"/>
				<Item Name="CFReleaseURL.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/CFReleaseURL.vi"/>
				<Item Name="CFStringCreate.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFString.llb/CFStringCreate.vi"/>
				<Item Name="CFStringGetCString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFString.llb/CFStringGetCString.vi"/>
				<Item Name="CFStringGetLength.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFString.llb/CFStringGetLength.vi"/>
				<Item Name="CFStringRef.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/CFString.llb/CFStringRef.ctl"/>
				<Item Name="CFURLCopyFileSystemPath.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/CFURLCopyFileSystemPath.vi"/>
				<Item Name="CFURLCreateWithFileSystemPath.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/CFURLCreateWithFileSystemPath.vi"/>
				<Item Name="CFURLCreateWithString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/CFURLCreateWithString.vi"/>
				<Item Name="CFURLGetString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/CFURLGetString.vi"/>
				<Item Name="CFURLRef.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/CFURLRef.ctl"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get File System Separator.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysinfo.llb/Get File System Separator.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get VI Library File Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get VI Library File Info.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Has LLB Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Has LLB Extension.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="InternetConfigLaunchURL.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/InternetConfigLaunchURL.vi"/>
				<Item Name="Librarian File Info In.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info In.ctl"/>
				<Item Name="Librarian File Info Out.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info Out.ctl"/>
				<Item Name="Librarian File List.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File List.ctl"/>
				<Item Name="Librarian.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open URL in Default Browser (path).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (path).vi"/>
				<Item Name="Open URL in Default Browser (string).vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser (string).vi"/>
				<Item Name="Open URL in Default Browser core.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser core.vi"/>
				<Item Name="Open URL in Default Browser.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/browser.llb/Open URL in Default Browser.vi"/>
				<Item Name="Parse State Queue__jki_lib_state_machine.vi" Type="VI" URL="/&lt;vilib&gt;/addons/_JKI Toolkits/State Machine/_JKI_lib_State_Machine.llb/Parse State Queue__jki_lib_state_machine.vi"/>
				<Item Name="Path to URL inner.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL inner.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="PathToUNIXPathString.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/CFURL.llb/PathToUNIXPathString.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="TRef Traverse for References.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse for References.vi"/>
				<Item Name="TRef Traverse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef Traverse.vi"/>
				<Item Name="TRef TravTarget.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/traverseref.llb/TRef TravTarget.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VI Scripting - Traverse.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/traverseref.llb/VI Scripting - Traverse.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="ApplicationServices.framework" Type="Document" URL="ApplicationServices.framework">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Carbon.framework" Type="Document" URL="Carbon.framework">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CoreFoundation.framework" Type="Document" URL="CoreFoundation.framework">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
