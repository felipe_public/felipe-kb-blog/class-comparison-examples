# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [1.1.0](https://gitlab.com/felipe_public/felipe-kb-blog/class-comparison-examples/compare/v1.0.0...v1.1.0) (2021-07-09)


### Features

* includes used classes in comparison ([4500770](https://gitlab.com/felipe_public/felipe-kb-blog/class-comparison-examples/commit/4500770f27db3b8106a77f1e728380133a73ee82))


### Docs

* includes readme file with instructions ([0775b58](https://gitlab.com/felipe_public/felipe-kb-blog/class-comparison-examples/commit/0775b5856d735fa16aaf63804f33a40e5607bbfe))


### CI

* removes first release flag from .gitlab-ci.yml ([6492181](https://gitlab.com/felipe_public/felipe-kb-blog/class-comparison-examples/commit/64921817c092e970abceb7bafd8ff71c93de1fe3))

## 1.0.0 (2021-07-09)


### CI

* includes automatic versioning files ([088b60b](https://gitlab.com/felipe_public/felipe-kb-blog/class-comparison-examples/commit/088b60bad4914b79191cbe323cac6c1b049523cc))


### Docs

* adds avatar file ([572911b](https://gitlab.com/felipe_public/felipe-kb-blog/class-comparison-examples/commit/572911b99f27f9770156fbed7e3fc6012f20b871))
